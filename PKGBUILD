# Maintainer: Balló György <ballogyor+arch at gmail dot com>
# Contributor: Kyle Keen <keenerd@gmail.com>
# Contributor: Lex Black <autumn-wind at web dot de>
# Contributor: Vladimir Kirillov <proger@wilab.org.ua>

pkgname=libpgm
pkgver=5.3.128
pkgrel=3
pkgdesc='Library implementing the Pragmatic General Multicast (PGM, RFC3208) reliable transport protocol (OpenPGM)'
arch=(x86_64)
url='https://github.com/steve-o/openpgm'
license=(LGPL-2.1-or-later)
depends=(glibc)
makedepends=(
  git
  python
)
source=("git+https://github.com/steve-o/openpgm.git#tag=release-${pkgver//./-}")
b2sums=(3718430386e4ca8b9e51dbd69a17d4bb71ef0169b8f78f66865aae3d5cca09f4bd63d18bac714b4e5a89ad09ecdb73a3fd8f7163c1806c0c1ff2f109a8898dcd)

prepare() {
  cd openpgm/openpgm/pgm

  # https://github.com/steve-o/openpgm/pull/66
  git cherry-pick -n 240634b1afb968a051f8c68696eae2a582a02450

  autoreconf -fi
}

build() {
  cd openpgm/openpgm/pgm
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var
  make
}

package() {
  cd openpgm/openpgm/pgm
  make DESTDIR="$pkgdir" install
}
